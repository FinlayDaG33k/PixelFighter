extends KinematicBody2D

const GAME_DIRECTION_UP = Vector2(0,-1)

export var movement_speed = 512
export var gravity_force = 64
export var jump_force = 1024

var is_Crouching = false
var facing_Direction = "Left"
var is_doing_action = false

# Punch timer
const punch_time = 0.15
var punch_timer = punch_time


var motion = Vector2()

func _ready():
	print("Game ready")
	pass


func _physics_process(delta):
	if punch_timer < punch_time:
		is_doing_action = true
		punch_timer += delta
		if punch_timer >= punch_time:
			is_doing_action = false
			pass


	###
	# Gravity
	###
	motion.y += gravity_force
	if(!is_doing_action):
		$Sprite.set_position(Vector2(0,0)) # Reset the sprite position
	
    
	   ###
	   # Movement
	   ###
    
    # Left-Right movement
	if(!is_doing_action):
		if(Input.is_action_pressed("ui_left")):
			if(!is_Crouching):
				# Left button is pressed
				motion.x = -movement_speed
			ChangeDirection("Left")
		elif(Input.is_action_pressed("ui_right")):
			if(!is_Crouching):
				# Right button is pressed
				motion.x = movement_speed
			ChangeDirection("Right")
		else:
			motion.x = 0
    
		# Jumping
		if is_on_floor():
			if(Input.is_action_just_pressed("ui_up") && !is_Crouching):
				motion.y = -jump_force
			# Crouching
			if Input.is_action_pressed("ui_down"):
				is_Crouching = true
				$Sprite.set_texture(load("res://Sprites/player_crouching.png")) # Change the sprite
				$Collision.set_scale(Vector2(4.25,5.4328485)) # Change the Collisionbox size
				$Collision.set_position(Vector2(0,57)) # Change Collisionbox position
			else:
				is_Crouching = false
				$Sprite.set_texture(load("res://Sprites/player_standing.png"))  # Change the sprite
				$Collision.set_scale(Vector2(4.25,10.865697)) # Change the Collisionbox size
				$Collision.set_position(Vector2(0,0)) # Change Collisionbox position
		motion = move_and_slide(motion,GAME_DIRECTION_UP)



		###
		# Attacks
		###
		if(Input.is_action_just_pressed("p1_sp")):
			punch_timer = 0
			if(is_Crouching):
				$Sprite.set_texture(load("res://Sprites/player_crouching_punch.png")) # Change the sprite
				match facing_Direction:
					"Left": 
						$Sprite.set_position(Vector2(-18,0)) # Re-center the sprite in the collisionbox
					"Right":
						$Sprite.set_position(Vector2(18,0)) # Re-center the sprite in the collisionbox
			else:
				$Sprite.set_texture(load("res://Sprites/player_standing_punch.png")) # Change the sprite
				match facing_Direction:
					"Left": 
						$Sprite.set_position(Vector2(-18,0)) # Re-center the sprite in the collisionbox
					"Right":
						$Sprite.set_position(Vector2(18,0)) # Re-center the sprite in the collisionbox


func ChangeDirection(direction):
	facing_Direction = direction
	if facing_Direction == "Left":
		$Sprite.set_flip_h(true)
	elif facing_Direction == "Right":
		$Sprite.set_flip_h(false)